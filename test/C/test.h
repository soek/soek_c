#ifndef TEST_H
#define TEST_H

void testDemo( int *nbTestSucces, int *nbTestTotal );

void testResultArray( int *nbTestSucces, int *nbTestTotal );

void testConfig( int *nbTestSucces, int *nbTestTotal );

void testAudio(int *nbTestSucces, int *nbTestTotal);

#endif
