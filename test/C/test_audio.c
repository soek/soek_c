#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "audio.h"


#define LIST_FILE_NAME "data/liste_base_audio"
#define DESC_FILE_NAME "data/base_descripteur_audio"
#define CONFIG_FILE_NAME "data/soek.config"

void testAudio_index(int *nbTestSucces, int *nbTestTotal)
{
	*nbTestSucces = *nbTestTotal = 0;
	
	int succes;

	remove(LIST_FILE_NAME);
	remove(DESC_FILE_NAME);
	remove(CONFIG_FILE_NAME);
	

	/* Test 1 */
	succes = audio_index("data/audio/corpus_fi.wav")==1;
	printf("1) audio_index(\"data/audio/corpus_fi.wav\") retourne  1: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;
	
	/* Test 2 */
	succes = audio_index("data/audio/notfile")==0;
	printf("2) audio_index(\"data/audio/notfile\") retourne  0: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;

	FILE* listFile = NULL;
	FILE* descFile = NULL;

	listFile = fopen( LIST_FILE_NAME, "r" );
	descFile = fopen( DESC_FILE_NAME, "r" );
	
	/* Test 3 */
	succes = listFile!=NULL ;
	printf("3) audio_index cree le fichier \"liste_base_audio\": %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;
	
	/* Test 4 */
	succes = descFile!=NULL ;
	printf("4) audio_index cree base_descripteur_audio: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;

	if( listFile==NULL )
		return;
	
	int currentId;
	char currentFileName[100];
	

	/* Test 5 */
	succes = 1;
	fseek( listFile, 0, SEEK_SET );
	while( succes && fscanf( listFile, "%d %s", &currentId, currentFileName )==2 )
		if( strcmp( "data/audio/notfile", currentFileName )==0 )
		       succes = 0;
	printf("5) \"data/audio/notfile\" n'existe pas dans liste_base_audio: %s\n", succes ? "Oui" : "Non" );
        (*nbTestTotal)++;
        if( succes ) (*nbTestSucces)++ ;

	/* Test 6 */
	succes = 0;
	fseek( listFile, 0, SEEK_SET );
	while( !succes && fscanf( listFile, "%d %s", &currentId, currentFileName )==2 )
		if( strcmp( "data/audio/corpus_fi.wav", currentFileName )==0 )
		       succes = 1;
	printf("6) \"data/audio/corpus_fi.wav\" existe dans liste_base_audio: %s\n", succes ? "Oui" : "Non" );
        (*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;

	int myFileId = currentId ;
	
	/* Test 7 */
	succes = 1;
	while( succes && fscanf( listFile, "%d %s", &currentId, currentFileName )==2 )
		if( strcmp( "data/audio/corpus_fi.wav", currentFileName )==0 )
			succes = 0;
	printf("7) \"data/audio/corpus_fi.wav\" existe pas plus d'une fois dans liste_base_audio: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
        if( succes ) (*nbTestSucces)++ ;


	/* Test 8 */
	succes = 1;
	fseek( listFile, 0, SEEK_SET );
	while( succes && fscanf( listFile, "%d %s", &currentId, currentFileName )==2 )
		if( myFileId==currentId && strcmp( "data/audio/corpus_fi.wav", currentFileName )!=0 )
		       succes = 0;
	printf("8) \"data/audio/corpus_fi.wav\" a un identifiant unique: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;

	if( descFile==NULL )
		return;

	int splitSize = 1024;
	int nbInterval = 16;

	/* Test 9 */
	char currentWord[10];
	succes = 0;
	while( !succes && fscanf( descFile, "%s", currentWord )==1  )
		if( currentWord[0]=='#' && atoi(currentWord+1) == myFileId ) 
			succes = 1 ;
	printf("9) L'identifiant existe dans \"base_descripteur_audio\": %s\n", succes ? "Oui" : "Non" );
        (*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;

	
	if( !succes )
		return;

	/* Test 10 */	
	int splitSum = 0;
	int currentValue;
	int i;
	for( i=0 ; i<nbInterval ; i++ )
	{
		fscanf( descFile, "%d", &currentValue );
		splitSum += currentValue;
	}

	succes = splitSum!=splitSize;
	printf("10) La taille des fenetres est respectee: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;

	fclose( listFile );
	fclose( descFile );
}      

void testAudio(int *nbTestSucces, int *nbTestTotal)
{
	testAudio_index( nbTestSucces, nbTestTotal );
}
