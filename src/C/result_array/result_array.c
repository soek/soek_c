/**@file result_array.c
 * 
 * @brief Type used to store research results
 * @author MALGOUYRES Alexandre
 * @date 07/12/2019
 */

#include "result_array.h"
#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Result* result_init(char* filePath, int distance) {
    Result* r;
	r = (Result*)malloc(sizeof(Result));
	r->filePath = filePath;
	r->distance = distance;
	return r;
}




ResultArray resultArray_init() {
	ResultArray r;
	r.size = 0;
	r.tab = (Result**)malloc(sizeof(Result*));
	return r;
}


int resultArray_isEmpty(ResultArray r) {
	return r.size == 0;
}

void resultArray_addResult (ResultArray* r, Result* my_result) {
	r->tab = (Result**)realloc(r->tab, (r->size+1) * sizeof(Result*));

	for(int i=0; i<r->size; i++) {

		//Vérifie si l'on doit insérer le nouveau résultat
		if(r->tab[i]->distance > my_result->distance) {
			int limite;
			
			limite = r-> size;
			r->size++;

			//Décale tous les résultats pour pouvoir insérer le nouveau
			for(int j=limite; j>i; j--) {
				r->tab[j] = r->tab[j-1];
			}

			r->tab[i] = my_result;

			return;
		}
	}

	//Si on n'a pas pu placer le nouveau résultat avant on le place à la fin du tableau
	r->tab[r->size] = my_result;
	r->size++;
}

Result* resultArray_getResult(ResultArray r, int index) {
	if(index>r.size-1) return NULL;
	return r.tab[index];
}


void resultArray_print(ResultArray r){
	FILE *f;
	f = fopen("data/resultatEncours","w+");
	for (int i=0; i<r.size; i++) {
		fprintf(f,"%s %d\n", resultArray_getResult(r,i)->filePath, resultArray_getResult(r,i)->distance);
	}

	system("mv data/resultatEncours data/resultat");
}

void resultArray_clear(ResultArray r) {
	for( int i = r.size -1 ; i >= 0 ; i-- ) {
		free(r.tab[i]);
	}
	r.size = 0;
}
