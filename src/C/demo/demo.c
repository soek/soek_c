#include <stdio.h>

/**@file demo.c
 * 
 * @brief An example of file in an other package
 * @author flabarrere
 */

int sayHelloWorld( int n )
{
	int i;
	for( i=0 ; i<n ; i++ )
		printf( "Hello world !\n" );

	return 0;
}
