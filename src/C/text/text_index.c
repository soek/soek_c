/**@file text_index.c
 * 
 * @brief Specific function for the function text_index
 * @author guilhem
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "text_index.h"

void wordList_print(WordList *list){
	Word *current=list->headWordList;
	printf("Affichage :\n");
	while (current!=NULL){
		printf("%s\t%d\n",current->word,current->nb_instance);
		current=current->next;
	}		
}

void wordList_add(WordList *list,char *currentWord){
	Word *newWord = malloc(sizeof(Word));
	if (newWord==NULL){
		exit(EXIT_FAILURE);
	}
	newWord->word = currentWord;
	newWord->nb_instance=1;
    newWord->next=list->headWordList;
    list->headWordList=newWord;
}

void wordList_increase_instance(WordList *list, char *currentWord){
    char *currentWordCpy = malloc(sizeof(char)*(strlen(currentWord)+1));
    strcpy(currentWordCpy,currentWord);
    int i;
    for (i=0;currentWordCpy[i]!='\0';i++){
        currentWordCpy[i]=tolower(currentWordCpy[i]);
    }
	Word *current =list->headWordList;
	int test=0;
	while (current!=NULL){
		if (strcmp(current->word,currentWordCpy)==0){
			current->nb_instance++;
			test=1;
		}
		current=current->next;
	}
	if(test==0){
		wordList_add(list,currentWordCpy);
	}
}

WordList *wordList_init(){
    WordList *list = malloc(sizeof(*list));
	list->headWordList=NULL;
	return list;
}

void wordList_sort_list(WordList *list){
    Word *new1=list->headWordList;
    Word *count;
    for (; new1->next!=NULL;new1=new1->next){
        for(count =new1->next; count !=NULL; count = count ->next){
            if (new1->nb_instance<count->nb_instance){
                int temp = new1->nb_instance;
                char *tempChar = new1->word;
                
                new1->nb_instance = count->nb_instance;
                new1->word = count ->word;
                
                count->nb_instance = temp;
                count->word = tempChar;
            }
        }
    }
}

int genererate_selected_word_count(WordList *list, unsigned int PARAM_INSTANCE_MIN, int PARAM_NOMBRE_MOT_MIN,int list_word_count){
    Word *current=list->headWordList;
    int selected_word_count = 0;
    int i;
    int max_loop = PARAM_NOMBRE_MOT_MIN;
    
	if(max_loop<=0){
        while (current!=NULL){
            if(current->nb_instance>=PARAM_INSTANCE_MIN){
                selected_word_count++;
            }
            current=current->next;
        }
	}
	else {
        if(PARAM_NOMBRE_MOT_MIN>list_word_count){
            max_loop=list_word_count;
        }
        for(i=0;i<max_loop;i++){
            if(current->nb_instance>=PARAM_INSTANCE_MIN){
                selected_word_count++;
            }
            current=current->next;
        }
    }
    return selected_word_count;
}

int genererate_article_word_count(WordList *list){
    Word *current=list->headWordList;
    int article_word_count =0;
    
    while(current!=NULL){
        article_word_count+=current->nb_instance;
        current=current->next;
    }
    return article_word_count;
        
}

int genererate_list_word_count(WordList *list){
    Word *current=list->headWordList;
    int list_word_count=0;
    while(current!=NULL){
        list_word_count++;
        current=current->next;
    }
    return list_word_count;
        
}

int genererate_ID(const char *fileName){
    int current_ID=0;
    int current_selected_word_count;
    int current_article_word_count;
    int max_ID=0;
    char current_fileName[1000];
    int cmp_result;
    
    FILE* file_liste_base=NULL;
    file_liste_base = fopen("data/liste_base_texte","a");
    fclose(file_liste_base);
    
    file_liste_base = fopen("data/liste_base_texte","r");
    if (file_liste_base!=NULL){
        while(!feof(file_liste_base)){
            fscanf(file_liste_base,"%d %s %d %d",&current_ID,current_fileName,&current_selected_word_count,&current_article_word_count);
            cmp_result=strcmp(current_fileName,fileName);
            if(current_ID>=max_ID){
                max_ID=current_ID;
            }
            if(cmp_result==0){
                return current_ID;
            }
            
        }
        fclose(file_liste_base);
    }
    max_ID++;
    return max_ID;
}

void genenerate_base_descripteur_texte(int ID,int selected_word_count, WordList *list){
    char base_descripteur_command[100000]={0};
    
    Word *current=list->headWordList;
    int i;
    
    FILE* file_base_desc=NULL;
    file_base_desc = fopen("data/base_descripteur_texte","a");
    fclose(file_base_desc);
    
    FILE* file_base_desc_cpy = NULL;
    file_base_desc_cpy = fopen("data/base_descripteur_texte_cpy", "w+");
    fclose(file_base_desc_cpy);
    
    int current_ID;
    char currentWord[100000];
    int current_nb_instance;
    
    file_base_desc = fopen("data/base_descripteur_texte","r");
	if (file_base_desc!=NULL){
        while(!feof(file_base_desc)){
            fscanf(file_base_desc,"%d %s %d\n",&current_ID,currentWord,&current_nb_instance);
            if(!(current_ID==ID || current_ID==0)){
                sprintf(base_descripteur_command,"printf \"%d %s %d\n\" >> data/base_descripteur_texte_cpy",current_ID,currentWord,current_nb_instance);
                system(base_descripteur_command);
                base_descripteur_command[0]='\0';
            }
        }
        fclose(file_base_desc);
    }
    for(i=0;i<selected_word_count;i++){
        sprintf(base_descripteur_command,"printf \"%d %s %d\n\" >> data/base_descripteur_texte_cpy",ID,current->word,current->nb_instance);
        system(base_descripteur_command);
        base_descripteur_command[0]='\0';
        current=current->next;
    }
    system("mv data/base_descripteur_texte_cpy data/base_descripteur_texte");
    
}

void genenerate_liste_base_texte(int ID, int selected_word_count, int article_word_count, const char *fileName){
    char liste_base_command[100000]={0};
    int cmp_result;
    
    int current_ID=0;
    int current_selected_word_count;
    int current_article_word_count;
    char current_fileName[1000];
    
    FILE* file_liste_base_cpy = NULL;
    file_liste_base_cpy = fopen("data/liste_base_texte_cpy", "w+");
    fclose(file_liste_base_cpy);
    
    FILE* file_liste_base=NULL;
    file_liste_base = fopen("data/liste_base_texte","r");
    
    if (file_liste_base!=NULL){
        while(!feof(file_liste_base)){
            fscanf(file_liste_base,"%d %s %d %d\n",&current_ID,current_fileName,&current_selected_word_count,&current_article_word_count);
            cmp_result=strcmp(current_fileName,fileName);
            if(!((cmp_result==0 && current_ID==ID) || current_ID==0)){
                sprintf(liste_base_command,"printf \"%d %s %d %d\n\" >> data/liste_base_texte_cpy",current_ID,current_fileName,current_selected_word_count,current_article_word_count);
                system(liste_base_command);
                liste_base_command[0]='\0';
            }
        }
        fclose(file_liste_base);
    }
    sprintf(liste_base_command,"printf \"%d %s %d %d\n\" >> data/liste_base_texte_cpy",ID,fileName,selected_word_count,article_word_count);
    system(liste_base_command);
    liste_base_command[0]='\0';
    
    system("mv data/liste_base_texte_cpy data/liste_base_texte");
}
