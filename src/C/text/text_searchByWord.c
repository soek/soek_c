/**@file text_searchByWord.c
 * 
 * @brief Specific function for the function text_seachByWord
 * @author guilhem
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "text_searchByWord.h"
#include "config.h"

void wordListSearch_add(WordListSearch *list,int current_ID, int current_nb_instance ){
	WordSearch *newWord = malloc(sizeof(WordSearch));
	if (newWord==NULL){
		exit(EXIT_FAILURE);
	}
	newWord->ID = current_ID;
	newWord->nb_instance=current_nb_instance;
    newWord->next=list->headWordList;
    list->headWordList=newWord;
}

WordListSearch *wordListSearch_init(){
    WordListSearch *list = malloc(sizeof(*list));
	list->headWordList=NULL;
	return list;
}

void wordListSearch_sort_list(WordListSearch *list){
    WordSearch *new1=list->headWordList;
    WordSearch *count;
    for (; new1->next!=NULL;new1=new1->next){
        for(count =new1->next; count !=NULL; count = count ->next){
            if (new1->nb_instance<count->nb_instance){
                int temp = new1->nb_instance;
                int tempID = new1->ID;
                
                new1->nb_instance = count->nb_instance;
                new1->ID = count ->ID;
                
                count->nb_instance = temp;
                count->ID = tempID;
                
            }
        }
    }
}

void generate_score(WordListSearch *list, ResultArray *files){
	WordSearch *current=list->headWordList;
    int nb_instance_max;
	nb_instance_max=current->nb_instance;
	while (current!=NULL){
        current->nb_instance=nb_instance_max-current->nb_instance;
        FILE* file_liste_base= NULL;
        int current_ID;
        char current_filePath[10000];
        int current_selected_word_count;
        int current_article_word_count;
        file_liste_base = fopen("data/liste_base_texte","r");
		Result *r;
        if (file_liste_base!=NULL){
			
            while(!feof(file_liste_base)){
				
                fscanf(file_liste_base,"%d %s %d %d\n",&current_ID,current_filePath,&current_selected_word_count,&current_article_word_count);
                if(current_ID==current->ID && current->nb_instance<=CONFIG.textDistanceMax){
					char *current_filePathCpy=malloc(sizeof(char)*(strlen(current_filePath)+1));
					strcpy(current_filePathCpy,current_filePath);
					int i;	
					i=current->nb_instance;
					r = result_init(current_filePathCpy, i);
					resultArray_addResult(files, r);
				}
			}
		}
		fclose(file_liste_base);
		current=current->next;
	}		
}

int generate_cmp_result(const char *wordToCompare,const char *userWord){
    int score;
    int cmp_result;
	char *userWordCpy = malloc(sizeof(char)*(strlen(userWord)+1));
    strcpy(userWordCpy,userWord);
	int i;
    for (i=0;userWordCpy[i]!='\0';i++){
        userWordCpy[i]=tolower(userWordCpy[i]);
    }
    cmp_result=strcmp(wordToCompare,userWordCpy);
    if (!(wordToCompare[0]==userWordCpy[0])){
        return -1;
    }
    if (cmp_result==0){
        return 0;
    }
    else{
        int diff;
        diff=strlen(wordToCompare)-strlen(userWordCpy);
        if (diff<0){
            diff=-1*diff;
        }
        score=100*diff;
        int j;
        int max;
        for(j=0;wordToCompare[j]!='\0' && userWordCpy[j]!='\0';j++){
            if(wordToCompare[j]!=userWordCpy[j]){
                score++;
            }
        }
    }
    return score;
}


