/**@file text.c
 * 
 * @brief Text section (index file, comparison beetween 2 files)
 * @author guilhem
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "text.h"
#include "text_index.h"
#include "text_searchByWord.h"
#include "config.h"
#include "useful.h"

int text_indexAll(){
	system("ls data/text/ > data/liste_nom_fichier");
    
    FILE* file_name_list = NULL;
    
    char current_fileName[1000]={0};
    char fileName[1000]={0};
    file_name_list = fopen("data/liste_nom_fichier","r");
    if (file_name_list!=NULL){
        while(!feof(file_name_list)){
            strcat(fileName,"data/text/");
            fscanf(file_name_list,"%s\n",current_fileName);
            strcat(fileName,current_fileName);
			text_index(fileName);
			fileName[0]='\0';
			}
        fclose(file_name_list);
    }
	else {
		fprintf(stderr,"Impossible d'ouvrir le fichier");
		return 0;
	}
    system("rm data/liste_nom_fichier");
    return 1;
}

int text_index( const char *filePath ){
    
    unsigned int nbCharacterMin = CONFIG.nbCharacterMin;
    unsigned int nbEmergenceMin = CONFIG.nbEmergenceMin;
    int nbWordMax = CONFIG.nbWordMax;
	
	WordList *list=wordList_init();
	FILE* current_file = NULL;
	char currentString[50];
	int currentChar =0 ;
    
    char commande[500];
    sprintf(commande, "cp %s data/text/ 2>/dev/null", filePath);
    system(commande);
    
    char fileIndexedPath[500];
    strcpy(fileIndexedPath, filePath);
    
    getIndexedPath(fileIndexedPath);
	
	current_file = fopen(fileIndexedPath, "r");
	if (current_file!=NULL){
		char *token;
        int i;
        for(i=0;i<10;i++){
            fscanf(current_file,"%*s");
        }
		while(!feof(current_file)){
			fscanf(current_file,"%s %d",currentString,&currentChar);
			char *str=strdup(currentString);
			while ((token=strtok_r(str,"<>-!?/.,()=';:\"",&str))){
				if(strcmp(token,"titre")&&strcmp(token,"auteur")&&strcmp(token,"resume")&&strcmp(token,"phrase")&&strcmp(token,"texte")&&strcmp(token,"article")&&strcmp(token,"date")&&strcmp(token,"très")&&strcmp(token,"été")&&strcmp(token,"être")&&strcmp(token,"cette")&&strcmp(token,"comme")&&strcmp(token,"même")&&strlen(token)>nbCharacterMin){
					wordList_increase_instance(list,token);
				}
			}
		}
		fclose(current_file);
	}
	else {
		fprintf(stderr,"Impossible d'ouvrir le fichier");
		return 0;
	}
	wordList_sort_list(list);
	int article_word_count;
	int list_word_count;
	int selected_word_count;
	int ID;
	article_word_count = genererate_article_word_count(list);
	list_word_count = genererate_list_word_count(list);
	selected_word_count = genererate_selected_word_count(list,nbEmergenceMin,nbWordMax,list_word_count);
	ID = genererate_ID(fileIndexedPath);
	genenerate_liste_base_texte(ID,selected_word_count,article_word_count,fileIndexedPath);
	genenerate_base_descripteur_texte(ID,selected_word_count,list);
	
	return 1;	
}

int text_searchByFile( const char *filePath, ResultArray *files ){
	
	
}

int text_searchByWord( const char *word, ResultArray *files ){
	
	WordListSearch *list;
	list = wordListSearch_init();
    
    FILE* file_base_desc = NULL;
    file_base_desc = fopen("data/base_descripteur_texte", "a");
    fclose(file_base_desc);
    
    FILE* file_liste_base = NULL;
    file_liste_base = fopen("data/liste_base_texte", "a");
    fclose(file_liste_base);
    
    int current_ID;
    char currentWord[100000];
    int current_nb_instance;
    int cmp_result;
    int test =0;
    file_base_desc = fopen("data/base_descripteur_texte", "r");
    if (file_base_desc!=NULL){
        while(!feof(file_base_desc)){
            fscanf(file_base_desc,"%d %s %d\n",&current_ID,currentWord,&current_nb_instance);
			cmp_result = generate_cmp_result(currentWord,word);
            if(cmp_result==1 || cmp_result==2 || cmp_result==100 || cmp_result==200 || cmp_result==300 || cmp_result==0 || cmp_result==101 || cmp_result==201 || cmp_result==301){
                wordListSearch_add(list,current_ID,current_nb_instance);
                test =1;
            }
        }
        fclose(file_base_desc);
    }
    else {
        fprintf(stderr,"Impossible d'ouvrir le fichier");
        return 0;
    }
    if (test==1){
        wordListSearch_sort_list(list);
        generate_score(list,files);
    }
    return 1;
}

int text_play( const char *filePath ){
	char command[1000];
	sprintf(command,"nano %s",filePath);
    system(command);
    return 1;
	
}

