/**@file useful.c
 * 
 * @brief Some useful fonctions
 * @author MALGOUYRES Alexandre
 * @date 16/01/2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "useful.h"


Type getType(const char filePath[]) {
	char c[3];
	int size = strlen(filePath);

	for(int i = 1 ; i <= 3 ; i++) {
		c[3-i] = filePath[size-i];
	}

	if(!strcmp(c, "xml")) {
		return TEXTE;
	} else if(!strcmp(c, "jpg")) {
		return RGB;
	} else if(!strcmp(c, "bmp")) {
		return BW;
	} else if(!strcmp(c, "wav") || !strcmp(c, "bin")) {
		return AUDIO;
	} else {
		return INCONNU;
	}
}

void getFileName(char filePath[]) {
	char d[500] = "/", b[500];
	strcpy(b, filePath);
	char *p = strtok(b, d);

	while(p != NULL) {
		strcpy(b, p);
		p = strtok(NULL, d);
	}

	strcpy(filePath, b);
}

void getIndexedPath(char filePath[]) {
	char c[500];

	switch(getType(filePath)) {
		case TEXTE :
			strcpy(c,"data/text/");
			break;
		case AUDIO :
			strcpy(c,"data/audio/");
			break;
		case RGB :
			strcpy(c,"data/image_rgb/");
			break;
		case BW :
			strcpy(c,"data/image_nb/");
			break;
		default :
			return;
	}

	getFileName(filePath);
	strcat(c, filePath);

	strcpy(filePath, c);
}

int getId(const char filePath[]) {
	Type fileType = getType(filePath);
	char fileName[500], buffer[500];
	FILE *f = NULL;
	int id = -1;

	strcpy(fileName, filePath);
	getIndexedPath(fileName);

	if(fileType == TEXTE) {
		f = fopen("data/liste_base_texte", "r");
	} else if(fileType == AUDIO) {
		f = fopen("data/liste_base_audio", "r");
	} else if(fileType == BW) {
		f = fopen("data/liste_base_image_nb", "r");
	} else if(fileType == RGB) {
		f = fopen("data/liste_base_image_rgb", "r");
	} else {
		return -1;
	}

	if(f == NULL) return -1;

	while(!feof(f)){
		if(fscanf(f, "%d %[^ ^\n] ", &id, buffer)) {
			if(strcmp(fileName, buffer) == 0) {
				fclose(f);
				return id;
			}
		}
	}
	fclose(f);
	return -1;
}

int isIndexed(char filePath[]) {
	char file[500];
	Type fileType;
	int d = 0;

	strcpy(file,filePath);
	fileType = getType(file);

	getIndexedPath(file);

	char pathListDesc[500];
	if(fileType == TEXTE) {
		strcpy(pathListDesc,"data/liste_base_texte");
	} else if(fileType == AUDIO) {
		strcpy(pathListDesc,"data/liste_base_audio");
	} else if(fileType == BW) {
		strcpy(pathListDesc,"data/liste_base_image_nb");
	} else if(fileType == RGB) {
		strcpy(pathListDesc,"data/liste_base_image_rgb");
	} else {
		return 0;
	}

	FILE *f;
	char buffer[500];
	f = fopen(pathListDesc, "r");
	if(f == NULL) {
		return 0;
	}

	while(!feof(f)){
		if(fscanf(f, " %[^ ^\n] ", buffer)) {
			if(strcmp(file,buffer) == 0) {
				fclose(f);
				return 1;
			}
		}
	}
	fclose(f);
	return 0;
}

int checkDataFile() {
	FILE * f = popen("ls", "r");
 
    char c[500] = "";
    while(!feof(f)){
    	fscanf(f, "\n%s", c);
    	if(strcmp(c, "data") == 0) {
    		pclose(f);
    		return 0;
    	}
    }

    pclose(f);
    return 1;
}