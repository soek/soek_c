/** @file result_array.h
 */

#ifndef RESULT_ARRAY_H
#define RESULT_ARRAY_H

/** @struct etResult
 *  
 *  @see Result
 */
/** @typedef Result
 *
 *  @brief a Result (aka struct etResult) associate a filepath with a distence with the compared file
 */
typedef struct etResult {
    char *filePath;              /**< Path to file */
    int distance;                /**< Score of distance between the file and the compared file (smaller is the score, better is the result) */
} Result;

/** @struct etResultArray
 *  
 *  @see ResultArray
 */
/** @typedef ResultArray
 *
 *  @brief A ResultArray (aka struct etResultArray) is a array of Result sorted by distance
 */
typedef struct etResultArray{
    Result **tab;                /**< Sorted array which contain result to print */
    int size;                    /**< Current size array */
} ResultArray;

/** @fn result_init( char* filePath, int distance)
 *  
 *  @brief Init a Result struct with defaults values
 *  @param filePath Value for filePath field in struct
 *  @param distance Value for distance field in struct
 *  @return Pointer on new Result
 *  @see Result
 */
Result* result_init( char* filePath, int distance);


/** @fn resultArray_init()
 *  
 *  @brief Init array empty ResultArray struct
 *  @return Pointer on new ResultArray
 */
ResultArray resultArray_init();

/** @fn resultArray_isEmpty(ResultArray r)
 *
 *  @brief Return 1 if the ResultArray array is empty
 *  @param r ResultArray to test
 *  @return 1 if empty, 0 else
 */
int resultArray_isEmpty(ResultArray r);

/** @fn resultArray_isFull(ResultArray r)
 *
 *  @brief Return 1 if the ResultArray is full
 *  @param r ResultArray to test
 *  @return 1 if full, 0 else
 */
int resultArray_isFull(ResultArray r);

/** @fn resultArray_addResult (ResultArray* r, Result* my_result)
 *
 *  @brief Add a Result in a ResultArray without breaking the sort by score
 *  @param r ResultArray which growing
 *  @param my_result Result to add
 */
void resultArray_addResult (ResultArray* r, Result* my_result);

/** @fn resultArray_getResult (ResultArray r, int index)
 *
 *  @brief Return the Result at index in ResultArray
 *  @param r ResultArray to pick
 *  @param index Index which will be returned
 *  @return Result at index in ResultArray r
 */
Result* resultArray_getResult (ResultArray r, int index);

/** @fn resultArray_print(ResultArray r)
 *
 *  @brief Print a ResultArray in stdout
 *  @param r ResultArray to print
 */
void resultArray_print(ResultArray r);

/** @fn resultArray_clear(ResultArray r)
 *
 *  @brief Free all the result of a resultArray
 *  @param r ResultArray to print
 */
void resultArray_clear(ResultArray r);

#endif
