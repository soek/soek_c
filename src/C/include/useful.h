/** @file useful.h
 */

#ifndef USEFUL_H
#define USEFUL_H


/** @enum Type
 *
 *  @brief Type is an enumeration with all the file types
 */
typedef enum { TEXTE = 0, AUDIO = 1, RGB = 2, BW = 3, INCONNU }Type;

/** @fn getType(char filePath[])
 *
 *  @brief Find the type of the file give in parameter
 *	@param filePath is a string with the file path
 *  @return Type of the parameter file
 */
Type getType(const char filePath[]);

/** @fn getFileName(char filePath[])
 *
 *  @brief Find the name of a file from is path
 *	@param filePath is a string with the file path. It will be replace by the name
 */
void getFileName(char filePath[]);

/** @fn getIndexedPath(char filePath[])
 *
 *  @brief Converte a file path to a path in the data base
 *	@param filePath is a string with the file path. It will be replace by the path of indexed file
 */
void getIndexedPath(char filePath[]);

/** @fn getId(char filePath[])
 *
 *  @brief find the id of a indexed file
 *	@param filePath is a string with the file path
 *	@return -1 if no id found
 *	@return id of the param file
 */
int getId(const char filePath[]);

/** @fn isIndexed(char filePath[])
 *
 *  @brief check if a file is indexed
 *	@param filePath is a string with the file path
 *	@return 0 if param file is not indexed
 *	@return 1 if param file is indexed
 */
int isIndexed(char filePath[]);

/** @fn checkDataFile()
 *
 *  @brief check if there is a data file
 *	@return 0 if there id no data file
 *	@return 1 if there is a data file
 */
int checkDataFile();

#endif
