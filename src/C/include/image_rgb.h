/** @file image_rgb.h
 */

#ifndef IMAGE_RGB_H
#define IMAGE_RGB_H

#include "result_array.h"

/** @fn imageRGB_indexAll()
 * 
 *  @brief Index all imageRGB file from data/imageRGB
 *  @return 0 if issue, 1 else
 */
int imageRGB_indexAll();

/** @fn imageRGB_index( const char *filePath )
 * 
 *  @brief Index the color image named filePath
 *  @param filePath Name of the file to index
 *  @return 0 if issue, 1 else
 */
int imageRGB_index( const char *filePath );

/** @fn imageRGB_searchByFile( const char *filePath, ResultArray *files )
 * 
 *  @brief Search the nearest color images to the image named filePath and return them in the ResultArray
 *  @param filePath Name of the file to compare
 *  @param files ResultArray to fill with result of the search
 *  @return 0 if issue, 1 else
 *  @see ResultArray
 */
int imageRGB_searchByFile( const char *filePath, ResultArray *files );

/** @fn imageRGB_play( const char *filePath )
 * 
 *  @brief Play the imageRGB file with an external program
 *  @param filePath Name of the file to play
 *  @return 0 if issue, 1 else
 */
int imageRGB_play( const char *filePath );

#endif
