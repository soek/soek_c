/** @file text_index.h
 */

#ifndef TEXT_INDEX_H
#define TEXT_INDEX_H

/** @struct andWord
 *	@typedef Word
 * 
 *  @brief This structure contains words : each word with its number of instance and a pointer to the next word
 *	@var Word::nb_instance
 *	Member 'nb_instance' contains the number of instance
 *	@var Word::word
 *	Member 'word' contains a word
 *	@var Word::next
 *	Member 'next' contains a pointer to the next word
 */
typedef struct andWord{
	int nb_instance;
	char *word;
	struct andWord * next;
}Word;

/** @struct anWordList
 *	@typedef WordList
 * 
 *  @brief This structure contains a pointer to the first element of the Word structure, it represents a list of word
 *	@var WordList::headWordlist
 *	Member 'headWordList' contains the pointer to first element of the Word structure
 */
typedef struct andWordList{
	Word *headWordList;
}WordList;

/** @fn wordList_print( WordList *list ) 
 * 
 *  @brief Print the list of Word for one file
 *  @param list The list of Word
 */
void wordList_print( WordList *list );

/** @fn wordList_add( WordList *list, char *currentWord ) 
 * 
 *  @brief Add at the top of the WordList a single word
 *  @param list The list of Word
 *	@param currentWord The current word read from the file
 */
void wordList_add( WordList *list, char *currentWord );

/** @fn wordList_increase_instance( WordList *list, char *currentWord ) 
 * 
 *  @brief Runs through the list of Word : if a word is already present increases the number of instance otherwise adds the word to the list of Word 
 *  @param list The list of Word
 *	@param currentWord The current word read from the file
 */
void wordList_increase_instance( WordList *list, char *currentWord );

/** @fn wordList_init()
 * 
 *  @brief Initialize the struct andWord and the struct andWordList
 *	@return list The list of Word
 */
WordList *wordList_init();

void wordList_sort_list(WordList *list);

int genererate_selected_word_count(WordList *list, unsigned int PARAM_INSTANCE_MIN, int PARAM_NOMBRE_MOT_MIN,int list_word_count);

int genererate_article_word_count(WordList *list);

int genererate_list_word_count(WordList *list);

int genererate_ID(const char *fileName);

void genenerate_base_descripteur_texte(int ID,int selected_word_count, WordList *list);

void genenerate_liste_base_texte(int ID, int selected_word_count, int article_word_count, const char *fileName);
										
	
#endif
