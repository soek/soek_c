#ifndef DEMO_H
#define DEMO_H

/**@file demo.h
 * 
 * @brief An example of file in an other package
 * @author flabarrere
 */


/**@fn int sayHelloWorld( int n )
 *
 * @brief Say "Hello world !" n time.
 * @param n Number of "Hello world !".
 * @return Return -1 if error, 0 else.
 */

int sayHelloWorld( int n );

#endif
