/** @file text_searchByWord.h
 */

#ifndef TEXT_SEARCHBYWORD_H
#define TEXT_SEARCHBYWORD_H 

#include "result_array.h"

typedef struct andWordSearch{
	int nb_instance;
	int ID;
	struct andWordSearch * next;
}WordSearch;

typedef struct andWordListSearch{
	WordSearch *headWordList;
}WordListSearch;

void wordListSearch_add(WordListSearch *list,int current_ID, int current_nb_instance);
WordListSearch *wordListSearch_init();
void wordListSearch_sort_list(WordListSearch *list);
void generate_score(WordListSearch *list, ResultArray *files);
int generate_cmp_result(const char *wordToCompare,const char *userWord);

#endif
