/** @file audio.c
 *
 *  @author flabarrere
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "audio.h"
#include "result_array.h"
#include "useful.h"
#include "config.h"

char* getBinFile( const char *originalFilePath )
{
	char *binFilePath = (char*) malloc( sizeof(char) * strlen(originalFilePath) );
	strcpy( binFilePath, originalFilePath );

	strcpy( strrchr( binFilePath, '.' ), ".bin");
	
	return binFilePath;
}


int audio_indexAll()
{
	char *ext;
	char fileName[100];
	char filePath[500];

	FILE * fichierDirectory = popen("ls data/audio", "r");

	while(!feof(fichierDirectory)){
		fscanf(fichierDirectory,"%s",filePath);
    		ext = strrchr(fileName, '.');
		if(ext != NULL && strcmp(ext,".wav")==0){  		
			strcpy(filePath,"data/audio/");
			strcat(filePath,fileName);		
			audio_index(filePath);
		}
    }

	// FILE * fichierDirectory = popen("find data/audio/ -name *.wav", "r");

	// while(fscanf(fichierDirectory,"%s",filePath)==1)
	// 	audio_index(filePath);

	return 0;
}

int audio_index( const char *filePath )
{
	const long int splitsize = CONFIG.splitSize;
	const long int nbInterval = CONFIG.nbInterval;
	
	if( strcmp( strrchr( filePath, '.' ), ".wav" )!=0 ){
		fprintf( stderr, "\a%s is not a wav file\n", filePath);
		return 0; /* this is not a wav file */
	}
	char *binFilePath = getBinFile(filePath);
	FILE *binFile = NULL;
	binFile = fopen( binFilePath , "r" );


	if( binFile==NULL )
	{
		fprintf( stderr, "Le fichier binnaire %s est introuvable\n", binFilePath );
		return 0;
	}

	FILE* listFile = NULL;
	listFile = fopen( "data/liste_base_audio", "a+" );
	rewind(listFile);
	
	int currentId=0 , fileId=-1;
	char currentFileName[100];

	while( fscanf( listFile, "%d %s", &currentId, currentFileName )==2 )
	{
		if( strcmp( currentFileName, filePath )==0 )
		{
			fileId = currentId;
		    break;
		}
	}
	if( fileId==-1 )
	{
		fileId= currentId+1 ;
		fprintf( listFile, "%d %s\n", fileId, filePath );
	}
	else
	{
		fprintf( stderr, "Le fichier %s est deja indexé\n", filePath );
		return 0;
	}

	fclose( listFile );
	FILE* descFile = NULL;
	descFile = fopen( "data/base_descripteur_audio", "a+" );
	
	fprintf( descFile, "\n#%d\n", fileId );
	
	double *split = (double*) malloc( splitsize * sizeof(double) );
	int *splitOcc = (int*) malloc( nbInterval * sizeof(int) );
	int i, effectiveSplitSize;
	
	
	while( effectiveSplitSize = fread( split, sizeof(double), splitsize, binFile ) )
	{

		for( i=0 ; i<nbInterval ; i++ )
			splitOcc[i] = 0;

		for( i=0 ; i<effectiveSplitSize ; i++ )
			splitOcc[(int)(nbInterval*split[i]/2)+nbInterval/2]++;
		for( i=0 ; i<nbInterval ; i++ )
			fprintf( descFile, "%d ", splitOcc[i]);
		fprintf( descFile, "\n");	
	}

	fclose(descFile);
	fclose(binFile);
	
	return 1;
}

int audio_compare_split( const int *split1, const int *split2 )
{
	const long int nbInterval = CONFIG.nbInterval;
	int i, diff=0;
	for( i=0 ; i<nbInterval ; i++ )
		diff += abs( split1[i]-split2[i] );
	return diff;
}

int audio_searchByFile( const char *filePath, ResultArray *files )
{
	const long int nbInterval = CONFIG.nbInterval;
	const long int threshold = CONFIG.threshold;

	FILE* descFile = NULL;
	descFile = fopen( "data/base_descripteur_audio", "r" );


	char currentWord[10];
	int i;


	int *comparedFileData = (int*) malloc( 4000*nbInterval*sizeof(int));
	int comparedFileDataSize=0;

	while( fscanf( descFile, "%s", currentWord )==1 )
		if( currentWord[0]=='#' && atoi(currentWord+1) == getId(filePath) ) 
			break ;

	for( i=0 ; fscanf( descFile, "%s", currentWord )==1 &&currentWord[0]!='#' ; i++)
	{
		comparedFileData[i]=atoi(currentWord);
		comparedFileDataSize++;
	}

	rewind(descFile);
	fscanf( descFile, "%s", currentWord );

	int *currentSplit = (int*) malloc( nbInterval*sizeof(int));
	int *diff = (int*) malloc( 4000*sizeof(int));
	int databaseFileId, currentId;
	int lineNumber, lineDone;

	while( !feof(descFile) )
	{
		databaseFileId = atoi(currentWord+1);
		
		for( i=0 ; i<4000 ; i++ ) diff[i]=0 ;
		
		for( lineNumber=0, lineDone=0 ;!lineDone; lineNumber++) /* for each line of database file */
		{
			for( i=0 ; i<nbInterval ; i++ ) /* scan a line*/
			{
				if( fscanf( descFile, "%s", currentWord )!=1 || currentWord[0]=='#')
				{
					lineDone = 1;
					break;
				}	
				currentSplit[i]=atoi(currentWord);
			}

			if(!lineDone)
				for( i=lineNumber-comparedFileDataSize/nbInterval+1 ; i<=lineNumber ; i++ ) /* then compare it to coresponding comparedFile line */
					diff[i]+= audio_compare_split( currentSplit, comparedFileData+((lineNumber-i)*nbInterval) );

		}

		int indexDiffMin = 0;
		for( i=1 ; i<(lineNumber-comparedFileDataSize/nbInterval) ; i++ )
			if( diff[i]<diff[indexDiffMin] )
				indexDiffMin = i;


		printf( "FL diff=%d n°%d %2.f%%\n", diff[indexDiffMin], indexDiffMin, indexDiffMin/(double)lineNumber*100 );


		FILE* listFile = fopen( "data/liste_base_audio", "r" );
		char *databaseFilePath = (char*) malloc( 200*sizeof(char) );
		while( fscanf( listFile, "%d %s", &currentId, databaseFilePath )==2 && currentId!=databaseFileId )
			continue;
		fclose(listFile);

		if( diff[indexDiffMin] < threshold*(comparedFileDataSize/nbInterval) )
			resultArray_addResult( files, result_init( databaseFilePath, diff[indexDiffMin] ));
	}

	fclose(descFile);

	return 1;
}

int audio_play( const char *filePath )
{
	char command[500] = "mocp --play ";
	strcat(command,filePath);
	return system(command)!=-1;
}

