#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "result_array.h"
#include "audio.h"
#include "image_bw.h"
#include "image_rgb.h"
#include "text.h"
#include "useful.h"

int printTexteDescripteur(int id) {
	FILE *f = NULL;
	FILE *fDesc = NULL;
	char c[20] = "\n", desc[500] = "";
	int idEnCours;
	char idChar[10];

	f = fopen("data/base_descripteur_texte", "r");
	if(f == NULL) return 1;

	fDesc = fopen("data/descripteurEncours", "w+");

	sprintf(idChar,"%d",id);
	strcat(c, idChar);

	while(!feof(f)) {
		if(fscanf(f, "\n%d", &idEnCours)) {
			fscanf(f, " %[^\n]", desc);
			if(idEnCours == id) {
				fprintf(fDesc,"%s\n", desc);
			}
		} else {
			fscanf(f, "%[^\n]", desc);
		}
	}
	fclose(fDesc);

	system("mv data/descripteurEncours data/descripteur");

	fclose(f);
	return 0;
}

int printAudioDescripteur(int id) {
	FILE *f = NULL;
	FILE *fDesc = NULL;
	char c[20] = "\n", desc[500] = "";
	int idEnCours, fichierEnCours = 0;
	char idChar[10];

	f = fopen("data/base_descripteur_audio", "r");
	if(f == NULL) return 1;

	fDesc = fopen("data/descripteurEncours", "w+");

	sprintf(idChar,"%d",id);
	strcat(c, idChar);

	while(!feof(f)) {
		if(fscanf(f, "\n#%d", &idEnCours)) {
			if(idEnCours == id) {
				fichierEnCours = 1;
			} else if(fichierEnCours) {
				fichierEnCours = 0;
				return 0;
			}
			fscanf(f, "\n%[^\n]", desc);
		} else {
			fscanf(f, "\n%[^\n]", desc);
		}
		if(fichierEnCours) {
			fprintf(fDesc,"%s\n", desc);
		}
	}
	fclose(fDesc);

	system("mv data/descripteurEncours data/descripteur");

	fclose(f);
	return 0;
}

int printBWDescripteur(int id) {
	char command[100] = "cat data/image_nb/base_descripteur_image_nb/", aux[50];

	sprintf(aux, "%d", id);

	strcat(command, aux);
	strcat(command, ".txt > data/descripteur");

	system(command);
}

int printRGBDescripteur(int id) {
	char command[100] = "cat data/image_rgb/base_descripteur_image_rgb/", aux[50];

	sprintf(aux, "%d", id);

	strcat(command, aux);
	strcat(command, ".txt > data/descripteur");

	system(command);
}

void getDescripteur(char filePath[]) {
	Type fileType = INCONNU;
	int (*print[])(int id) = { printTexteDescripteur, printAudioDescripteur, printRGBDescripteur, printBWDescripteur};

	if(!isIndexed(filePath)) {
		FILE *f;
		f = fopen("data/descripteurEncours", "w+");
		fprintf(f,"Erreur\nDocument non indexer");
		fclose(f);
		system("mv data/descripteurEncours data/descripteur");
		return;
	}

	fileType = getType(filePath);

	if(fileType == INCONNU) {
		FILE *f;
		f = fopen("data/descripteurEncours", "w+");
		fprintf(f,"Erreur\nDocument non indexer\n");
		fclose(f);
		system("mv data/descripteurEncours data/descripteur");
		return;
	}

	(*print[fileType])(getId(filePath));
}


void indexer(char filePath[]) {
	int (*index[])(const char *filePath) = { text_index, audio_index, imageRGB_index, imageBW_index };
	int (*indexAll[])() = { text_indexAll, audio_indexAll, imageRGB_indexAll, imageBW_indexAll };
	Type fileType = INCONNU;
	FILE *f = NULL;

	f = NULL;

	f = fopen(filePath, "r");
	if(f == NULL) {
		if(strcmp(filePath,"all") == 0) {
			for(int i=0; i<4; i++) {
				(*indexAll[i])();
				return;
			}
		} else if(!strcmp(filePath, "audio")) {
			audio_indexAll();
		} else if(!strcmp(filePath, "text")) {
			text_indexAll();
		} else if(!strcmp(filePath, "rgb")) {
			imageRGB_indexAll();
		} else if(!strcmp(filePath, "bw")) {
			imageBW_indexAll();
		}
	} else {
		fileType = getType(filePath);
		if(fileType != INCONNU) {
			(*index[fileType])(filePath);
		}	
	}

	return;
}

void comparer(char filePath[]) {
	ResultArray result = resultArray_init();
	FILE *f = NULL;
	Type fileType;
	char c[500] = "";

	int (*index[])(const char *filePath) = { text_index, audio_index, imageRGB_index, imageBW_index };
	int (*searchByFile[])(const char *filePath, ResultArray *files) = { text_searchByFile, audio_searchByFile, imageRGB_searchByFile, imageBW_searchByFile };

	f = fopen(filePath, "r");
	if(f == NULL) {
		resultArray_print(result);
		resultArray_clear(result);
		return;
	}
	fclose(f);

	fileType = getType(filePath);

	if(fileType == INCONNU) {
		resultArray_print(result);
		resultArray_clear(result);
		return;
	}

	if(isIndexed(filePath) == 0) {
		(*index[fileType])(filePath);
	}

	strcpy(c, filePath);

	getIndexedPath(c);
	(*searchByFile[fileType])(c, &result);

	resultArray_print(result);
	resultArray_clear(result);
	return;
}


void chercher(char word[]) {
	ResultArray result = resultArray_init();

	text_searchByWord(word, &result);

	resultArray_print(result);
	resultArray_clear(result);
}

void launch(int argc, char *argv[]) {
	if(checkDataFile()) {
		system("make data");
	}
	config_loadFile();

	if( argc > 2 && strcmp( argv[1], "comparer" ) == 0 && strcmp(argv[2], "" ) != 0) {
		comparer(argv[2]);
	} else if( argc > 2 && strcmp( argv[1], "chercher" ) == 0 && strcmp(argv[2], "" ) != 0) {
		chercher(argv[2]);
	} else if( argc > 2 && strcmp(argv[1], "indexer" ) == 0 && strcmp(argv[2], "" ) != 0) {
		indexer(argv[2]);
	} else if( argc > 2 && strcmp(argv[1], "descripteur" ) == 0 && strcmp(argv[2], "" ) != 0) {
		getDescripteur(argv[2]);
	}
}

void main (int argc, char *argv[]) {
	launch (argc, argv);
}
