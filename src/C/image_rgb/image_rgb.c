/**@file image_rgb.c
 * 
 * @brief Entity that manipulates RGB pictures
 * @author CLERMONT-PEZOUS Nicolas
 * @date 20/12/2019
 */

#include "image_rgb.h"

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <useful.h>

#include <config.h>

int imageRGB_indexAll() {
	char *ext;
	char fileName[100];
	char filePath[500];

	FILE * fichierDirectory = popen("ls data/image_rgb", "r");

	while(!feof(fichierDirectory)){
		fscanf(fichierDirectory,"%s",fileName);
    		ext = strrchr(fileName, '.');
		if(ext != NULL && strcmp(ext+1,"jpg")==0){  		
			strcpy(filePath,"data/image_rgb/");
			strcat(filePath,fileName);	
			imageRGB_index(filePath);
		}
    	}
    	pclose(fichierDirectory);

	return 0;
}

int imageRGB_index(const char *filePath) {
	FILE *fichierTxt = NULL;
	FILE *fichierDesc = NULL;
	FILE *fichierListe = NULL;

	FILE *checkFile;
	checkFile = fopen(filePath, "r");
	if(checkFile == NULL){
        	return 1;
    	}
	fclose(checkFile);

	system("mkdir data/image_rgb/base_descripteur_image_rgb 2>/dev/null");

	FILE *checkListe;
	checkListe = fopen("data/liste_base_image_rgb", "a");
	if(checkListe == NULL){
		fprintf(stderr,"Fichier liste non créé!\n");
        	return 1;
    	}
	fclose(checkListe);

	char filePathInBase[500];
	strcpy(filePathInBase,filePath);

	if(isIndexed(filePathInBase)){
		return 0;	
	}

	getIndexedPath(filePathInBase);

	char command[500] = "cp ";
	strcat(command,filePath);
	strcat(command," data/image_rgb/ 2>/dev/null");
	system(command);

	char txtFilePathInBase[500];
	strcpy(txtFilePathInBase,filePathInBase);
	int l = strlen(txtFilePathInBase);
	txtFilePathInBase[l-3] = '\0';
	strcat(txtFilePathInBase,"txt");

	char txtFilePath[500];
	strcpy(txtFilePath,filePath);
	l = strlen(txtFilePath);
	txtFilePath[l-3] = '\0';
	strcat(txtFilePath,"txt");

	strcpy(command,"cp ");
	strcat(command,txtFilePath);
	strcat(command," data/image_rgb/ 2>/dev/null");
	system(command);

	fichierListe = fopen("data/liste_base_image_rgb","r+");

	if(fichierListe == NULL) {
		fprintf(stderr,"Impossible d'ouvrir le fichier liste\n");
		return 1;
	}

	int id = 0;
	char buffer[500];
	
	while(!feof(fichierListe)){
		fscanf(fichierListe,"\n%d",&id);
		fscanf(fichierListe,"%s",buffer);
		if(strcmp(filePath,buffer) == 0){
			fclose(fichierListe);
			return 0;
		}
	}

	id++;
	char idToString[10];
	sprintf(idToString,"%d",id);

	char filePathDesc[500] = "data/image_rgb/base_descripteur_image_rgb/";
	strcat(filePathDesc,idToString);
	strcat(filePathDesc,".txt");

	fichierDesc = fopen(filePathDesc,"w");

	if(fichierDesc == NULL) {
		fclose(fichierListe);
		fprintf(stderr,"Impossible d'ouvrir le fichier desc\n");
		return 1;
	}

	fprintf(fichierListe,"%d ",id);
	fprintf(fichierListe,"%s\n",filePathInBase);

	int buff;
	int nbLi;
	int nbCol;
	int nbComp;
		
	fichierTxt = fopen(txtFilePath,"r");

	if(fichierTxt == NULL) {
		fclose(fichierListe);
		fclose(fichierDesc);
		fprintf(stderr,"Impossible d'ouvrir le fichier txt\n");
		return 1;
	}

	fscanf(fichierTxt,"%d",&buff);
	nbLi = buff;
	fscanf(fichierTxt,"%d",&buff);
	nbCol = buff;
	fscanf(fichierTxt,"%d",&buff);
	nbComp = buff;

	int tabDesc[64] = {0};
	int tabPreQuant[nbComp][nbLi][nbCol][2];

	int fmsb;
	int smsb;

	for(int k=0;k<nbComp;k++){
		for(int i=0;i<nbLi;i++){
			for(int j=0;j<nbCol;j++){
				fscanf(fichierTxt,"%d",&buff);

				fmsb = buff/128;
				if(fmsb == 1)
					smsb = (buff-128)/64;
				else
					smsb = buff/64;

				tabPreQuant[k][i][j][0] = fmsb;
				tabPreQuant[k][i][j][1] = smsb;		
			}
		}
	}
	
	for(int i=0;i<nbLi;i++){
		for(int j=0;j<nbCol;j++){
			int pixel = (tabPreQuant[0][i][j][0] * 32 + tabPreQuant[0][i][j][1] * 16
				    +tabPreQuant[1][i][j][0] * 8 + tabPreQuant[0][i][j][1] * 4
				    +tabPreQuant[2][i][j][0] * 2 + tabPreQuant[0][i][j][1] * 1);
			tabDesc[pixel] ++;
		}
	}

	for(int i=0;i<64;i++){
		fprintf(fichierDesc,"%d\n",tabDesc[i]);	
	}
	
	fclose(fichierListe);
	fclose(fichierTxt);
	fclose(fichierDesc);

	return 0;
}

int imageRGB_searchByFile(const char *filePath, ResultArray *files) {
	char filePathInBase[500];
	strcpy(filePathInBase,filePath);

	if(!isIndexed(filePathInBase)){
		imageRGB_index(filePathInBase);	
	}

	getIndexedPath(filePathInBase);

	FILE *fichierListe = NULL;
	FILE *fichierDesc = NULL;

	fichierListe = fopen("data/liste_base_image_rgb","r+");

	if(fichierListe == NULL) {
		fprintf(stderr,"Impossible d'ouvrir le fichier liste\n");
		return 1;
	}

	int descValues[63];
	int indexId;
	char buffer[500];
	
	while(!feof(fichierListe)){
		fscanf(fichierListe,"%d",&indexId);
		fscanf(fichierListe,"%s\n",buffer);

		if(strcmp(filePathInBase,buffer) == 0){
			char descPath[500] = "data/image_rgb/base_descripteur_image_rgb/";
			char idToString[10];
			sprintf(idToString,"%d",indexId);

			strcat(descPath,idToString);
			strcat(descPath,".txt");	

			fichierDesc = fopen(descPath,"r");

			if(fichierDesc == NULL) {
				fclose(fichierListe);
				fprintf(stderr,"Impossible d'ouvrir le fichier descripteur\n");
				return 1;
			}

			for(int i=0;i<64;i++){
				fscanf(fichierDesc,"%d",&descValues[i]);
			}

			fclose(fichierDesc);
		}
	}
	fclose(fichierListe);

	FILE *fichierListe2 = NULL;
	fichierListe2 = fopen("data/liste_base_image_rgb","r+");

	if(fichierListe2 == NULL) {
		fprintf(stderr,"Impossible d'ouvrir le fichier liste\n");
		return 1;
	}

	while(!feof(fichierListe2)){
		char *buff = malloc(sizeof(filePath));

		fscanf(fichierListe2,"%d",&indexId);
		fscanf(fichierListe2,"%s\n",buff);

		if(strcmp(filePathInBase,buff) != 0){
			char descPath[500] = "data/image_rgb/base_descripteur_image_rgb/";
			char idToString[10];
			sprintf(idToString,"%d",indexId);			

			strcat(descPath,idToString);
			strcat(descPath,".txt");		

			FILE *fichierDescToComp;
			fichierDescToComp = fopen(descPath,"r");

			if(fichierDescToComp == NULL) {
				fclose(fichierListe2);
				fprintf(stderr,"Impossible d'ouvrir le fichier descripteur\n");
				return 1;
			}
			
			int pixelValue;
			int distance = 0;
			
			for(int i=0;i<64;i++){
				fscanf(fichierDescToComp,"%d",&pixelValue);
				int v = pixelValue - descValues[i];
				distance += abs(v);
			}
		
			if(distance <= CONFIG.rgbDistanceMax){
				Result *r = result_init(buff,distance);
				resultArray_addResult(files,r);
			}

			fclose(fichierDescToComp);
		}
	}
	fclose(fichierListe2);
	return 0;
}

int imageRGB_play(const char *filePath) {
	char command[500] = "eog ";
	strcat(command,filePath);
	strcat(command," &");
	system(command);
	return 0;
}
